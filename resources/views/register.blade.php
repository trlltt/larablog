<!DOCTYPE html>
<html>
<head>
	<title>SignUp</title>
</head>
<body>
	<h2>Buat Account Baru!</h2>
	<h4>Sign Up Form</h4>

	<form action="/welcome" method="post">
		@csrf
		<label>First name:</label><br>
		<input type="text" name="first" id="firstname"><br><br>

		<label>Last name:</label><br>
		<input type="text" name="last" id="lastname"><br><br>

		<label>Gender:</label><br>
		<input type="radio" name="gender" value="0"> Male <br>
		<input type="radio" name="gender" value="1"> Female<br>
		<input type="radio" name="gender" value="2"> Other<br><br>

		<label>Nationality:</label><br>
		<select>
			<option value="indonesian">Indonesian</option>
			<option value="singaporean">Singaporean</option>
			<option value="malaysian">Malaysian</option>
			<option value="australian">Australian</option>
		</select> <br><br>

		<label>Language Spoken:</label><br>
		<input type="checkbox" name="language" value="0"> Bahasa Indonesia <br>
		<input type="checkbox" name="language" value="1"> English <br>
		<input type="checkbox" name="language" value="1"> Other <br><br>

		<label>Bio:</label><br>
		<textarea id="bio" cols="30" rows="9"></textarea><br><br>

		<input type="submit" value="SignUp">
	</form>

</body>
</html>